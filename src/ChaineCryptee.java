
public class ChaineCryptee {
	
	private String Cryptee;
	private int Decalage;
	
	
	
	public static ChaineCryptee fromCrypted(String Cryptee,int Decalage)
	{
		int i;
		char c;
		String clair = "";
		for(i=0;i<Cryptee.length(); i++)
		{
			c = decaleCaractere(Cryptee.charAt(i),26 - Decalage);
			clair += c;
		}
		
		return new ChaineCryptee(clair,Decalage);
	}
	
	public static ChaineCryptee fromDecrypted(String Decryptee,int Decalage)
	{
		return new ChaineCryptee(Decryptee,Decalage);
	}
	
	
	private ChaineCryptee(String clair, int Decalage)
	{
		this.Decalage = Decalage;
		
		int i;
		char c;
		String cryptee = "";
		for(i=0;i<clair.length(); i++)
		{
			c = decaleCaractere(clair.charAt(i),Decalage);
			cryptee += c;
		}
		
		this.Cryptee = cryptee;
	}
	
	
	
	private static char decaleCaractere(char c, int decalage) {
		return (c < 'A' || c > 'Z')? c : (char)(((c - 'A' + decalage)%26)+ 'A');
	}
	
	public String Crypte()
	{
		return Cryptee;
	}

	
	public String Decrypte()
	{
		int i;
		char c;
		String decryptee = "";
		for(i=0;i<Cryptee.length(); i++)
		{
			c = decaleCaractere(Cryptee.charAt(i),26 - Decalage);
			decryptee += c;
		}
		return decryptee;
		
	}

}
