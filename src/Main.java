
public class Main {
	/** 
	 * La chaine null est gerée grace à un try catch  
	 *  **/
	
	
	
	public static void main(String[] args)
	{
		try
		{
			
			ChaineCryptee cypher = ChaineCryptee.fromDecrypted("DEF\n", 3);
			ChaineCryptee cypher2 = ChaineCryptee.fromCrypted("DEF\n", 3);

			System.out.print(cypher.Crypte());
			System.out.print(cypher.Decrypte());
			System.out.print(cypher2.Crypte());
			System.out.print(cypher2.Decrypte());
		}
		catch(NullPointerException e)
		{
			System.out.println("La chaine est NULL");
		}
	}
}
